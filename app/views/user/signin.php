<div class="container mt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-6">
            <h3>Sign In</h3>

            <hr />
            <form class="mt-2" action="<?= BASE_URL; ?>/user/signin" method="POST">
                <div class="form-outline mb-4">
                    <label class="form-label">Username</label>
                    <input type="text" class="form-control" name="username" required />
                </div>
                <div class="form-outline mb-4">
                    <label class="form-label">Password</label>
                    <input type="password" class="form-control" name="password" required />
                </div>
                <button type="submit" class="btn btn-primary btn-block mb-4">Submit</button>
            </form>
            <hr>
            <a href="signup">Register an account?</a>

        </div>
    </div>
</div>