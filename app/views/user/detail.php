<div class="container mt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-6">
            <div class="card">
                <img src="<?= BASE_URL; ?>/img/profile.png" class="d-flex mx-auto rounded-circle shadow" alt="raze" style="width: 250px;">
                <div class="card-body">
                    <h3 class="card-title"><?= $data['res']['name']; ?></h3>
                    <hr/>
                    <p class="fs-5 card-text">Level: <?= $data['res']['level']; ?></p>
                    <p class="fs-5 card-text">Username: <?= $data['res']['username']; ?></p>
                    <p class="fs-5 card-text">Email: <?= $data['res']['email']; ?></p>
                    <p class="fs-5 card-text">Phone: <?= $data['res']['phone']; ?></p>
                    <input class="fs-5 card-text" type="password" readonly value="<?= $data['res']['password']; ?>" />
                </div>
            </div>
        </div>
    </div>
</div>