<div class="container mt-5">
  <div class="row d-flex justify-content-center">
    <div class="col-6">
      <h3>ADD USER</h3>

      <hr />
      <form class="mt-2" action="<?= BASE_URL; ?>/user/add/" method="POST">
        <div class="form-outline mb-4">
          <label class="form-label" for="name">Name</label>
          <input type="text" class="form-control" name="name" id="name" required" />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label" for="username">Username</label>
          <input type="text" class="form-control" name="username" id="username" required" />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label" for="phone">Number</label>
          <input type="number" class="form-control" name="phone" id="phone" required" />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label" for="email">Email</label>
          <input type="email" class="form-control" name="email" id="email" required" />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label" for="password">Password</label>
          <input type="password" class="form-control" name="password" id="password" required" />
        </div>
        <button type="submit" class="btn btn-primary btn-block mb-4">Submit</button>
      </form>

    </div>
  </div>
</div>