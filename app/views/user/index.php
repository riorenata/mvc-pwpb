<div class="container mt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-12">
            <h3>USER LIST</h3>
            <a href="<?= BASE_URL; ?>/user/add" class="btn btn-success my-2">Add User</a>
            <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Level</th>
                <th scope="col">Name</th>
                <th scope="col" class="col-4">Method</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data['res'] as $res): ?>
                <tr>
                <th scope="row"><?= $res['id']; ?></th>
                <td><?= $res['level']; ?></td>
                <td><?= $res['name']; ?></td>
                <td>
                    <a href="<?= BASE_URL; ?>/user/detail/<?= $res['id']; ?>" class="btn btn-primary">Detail</a>
                    <a href="<?= BASE_URL; ?>/user/edit/<?= $res['id']; ?>" class="btn btn-warning m-2">Edit</a>
                    <a href="<?= BASE_URL; ?>/user/delete/<?= $res['id']; ?>" class="btn btn-danger">Delete</a>
                </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            </table>
        </div>
    </div>
</div>