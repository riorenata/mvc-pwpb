<div class="container mt-5">
  <div class="row d-flex justify-content-center">
    <div class="col-6">
      <h3>EDIT : <?= $data['res']['name']; ?></h3>

      <hr />
      <form class="mt-2" action="<?= BASE_URL; ?>/user/edit/<?= $data['res']['id']; ?>" method="POST">
        <input type="hidden" name="id" value="<?= $data['res']['id']; ?>" />
        <div class="form-outline mb-4">
          <label class="form-label">Level</label>
          <select name="level" id="level">
            <option value="<?= $data['res']['level']; ?>" selected><?= $data['res']['level']; ?></option>
            <option value="admin">Admin</option>
            <option value="staff">Staff</option>
            <option value="user">User</option>
          </select>
        </div>
        <div class="form-outline mb-4">
          <label class="form-label">Name</label>
          <input type="text" class="form-control" name="name" value="<?= $data['res']['name']; ?>" />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label">Username</label>
          <input type="text" class="form-control" name="username" value="<?= $data['res']['username']; ?>" />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label">Phone</label>
          <input type="number" class="form-control" name="phone" value="<?= $data['res']['phone']; ?>" />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label">Email</label>
          <input type="email" class="form-control" name="email" value="<?= $data['res']['email']; ?>" />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label">Password</label>
          <input type="password" class="form-control" name="password" value="<?= $data['res']['password']; ?>" />
        </div>
        <button type="submit" class="btn btn-primary btn-block mb-4">Submit</button>
      </form>

    </div>
  </div>
</div>