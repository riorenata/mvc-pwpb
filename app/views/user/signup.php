<div class="container mt-5">
  <div class="row d-flex justify-content-center">
    <div class="col-6">
      <h3>Sign Up</h3>

      <hr />
      <form class="mt-2" action="<?= BASE_URL; ?>/user/signup" method="POST">
        <div class="form-outline mb-4">
          <label class="form-label">Name</label>
          <input type="text" class="form-control" name="name" required />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label">Username</label>
          <input type="text" class="form-control" name="username" required />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label">Email</label>
          <input type="email" class="form-control" name="email" required />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label">Phone</label>
          <input type="number" class="form-control" name="phone" required />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label">Password</label>
          <input type="password" class="form-control" name="password" required />
        </div>
        <div class="form-outline mb-4">
          <label class="form-label">Confirm Password</label>
          <input type="password" class="form-control" name="password2" required />
        </div>
        <button type="submit" class="btn btn-primary btn-block mb-4">Submit</button>
      </form>
      <hr>
      <a href="signin" class="">Already have account?</a>

    </div>
  </div>
</div>