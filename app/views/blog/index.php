<div class="container mt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-12">
            <h3>BLOG LIST</h3>
            <a href="<?= BASE_URL; ?>/blog/add" class="btn btn-success my-2">Add Blog</a>
            <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Writer</th>
                <th scope="col">Title</th>
                <th scope="col">News</th>
                <th scope="col" class="col-4">Method</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data['res'] as $res): ?>
                <tr>
                <th scope="row"><?= $res['id']; ?></th>
                <td><?= $res['writer']; ?></td>
                <td><?= $res['title']; ?></td>
                <td><?= $res['news']; ?></td>
                <td>
                    <a href="<?= BASE_URL; ?>/blog/detail/<?= $res['id']; ?>" class="btn btn-primary">Detail</a>
                    <a href="<?= BASE_URL; ?>/blog/edit/<?= $res['id']; ?>" class="btn btn-warning m-2">Edit</a>
                    <a href="<?= BASE_URL; ?>/blog/delete/<?= $res['id']; ?>" class="btn btn-danger">Delete</a>
                </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            </table>
        </div>
    </div>
</div>