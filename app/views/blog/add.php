<div class="container mt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-6">
            <h3>ADD BLOG</h3>

            <hr/>
            <form class="mt-2" action="<?= BASE_URL; ?>/blog/add/" method="POST">
            <div class="form-outline mb-4">
                <label class="form-label">Writer</label>
                <input type="text" class="form-control" name="writer" required"/>
            </div>
            <div class="form-outline mb-4">
                <label class="form-label">Title</label>
                <input type="text" class="form-control" name="title" required"/>
            </div>
            <div class="form-outline mb-4">
                <label class="form-label">News</label>
                <textarea class="form-control" rows="4" name="news" required></textarea>
            </div>
            <button type="submit" class="btn btn-primary btn-block mb-4">Submit</button>
            </form>

        </div>
    </div>
</div>