<div class="container mt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-6">
            <h3>EDIT : <?= $data['res']['title']; ?></h3>

            <hr />
            <form class="mt-2" action="<?= BASE_URL; ?>/blog/edit/<?= $data['res']['id']; ?>" method="POST">
                <input type="hidden" name="id" value="<?= $data['res']['id']; ?>" />
                <input type="hidden" name="username" value="<?= $_SESSION['user']['username']; ?>" />
                <div class="form-outline mb-4">
                    <label class="form-label">Writer</label>
                    <input type="text" class="form-control" name="writer" value="<?= $data['res']['writer']; ?>" />
                </div>
                <div class="form-outline mb-4">
                    <label class="form-label">Title</label>
                    <input type="text" class="form-control" name="title" value="<?= $data['res']['title']; ?>" />
                </div>
                <div class="form-outline mb-4">
                    <label class="form-label">News</label>
                    <textarea class="form-control" rows="4" name="news"><?= $data['res']['news']; ?></textarea>
                </div>
                <button type="submit" class="btn btn-primary btn-block mb-4">Submit</button>
            </form>

        </div>
    </div>
</div>