<div class="container mt-5">
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <div class="card">
                <img src="http://placekitten.com/300/200" class="card-img-top" alt="Fissure in Sandstone"/>
                <div class="card-body">
                    <h3 class="card-title"><?= $data['res']['title']; ?></h3>
                    <p class="fs-6 fst-italic">writer: <?= $data['res']['writer']; ?></p>
                    <hr/>
                    <p class="fs-5 card-text"><?= $data['res']['news']; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>