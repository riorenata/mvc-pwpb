<?php

require_once '../app/controllers/Blog.php';

class Home extends Controller {

  // public function index() {
  //   $data = [
  //     'title' => 'Home',
  //     'name' => 'Raze'
  //   ];
  //   $this->view('templates/header', $data);
  //   $this->view('home/index', $data);
  //   $this->view('templates/footer');
  // }

  public function __construct(){
    if (!$this->model('User_model')->isLoggedIn()) exit(header('Location: '.BASE_URL.'/user/signin'));
  }
  
  public function index(){
    $blog = new Blog;
    $blog->index();
    // var_dump($_SESSION['user']);
  }

}