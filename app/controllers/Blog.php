<?php

class Blog extends Controller
{
    public function __construct()
    {
        if (!$this->model('User_model')->isLoggedIn()) exit(header('Location: ' . BASE_URL . '/'));
    }

    public function index()
    {
        $data = [
            'title' => 'Blog',
            'res' => $this->model('Blog_model')->getAllBlog()
        ];
        $this->view('templates/header', $data);
        $this->view('blog/index', $data);
        $this->view('templates/footer');
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Blog',
            'res' => $this->model('Blog_model')->getBlogById($id)
        ];
        $this->view('templates/header', $data);
        $this->view('blog/detail', $data);
        $this->view('templates/footer');
    }

    public function add()
    {
        $data = [
            'title' => 'Add Blog'
        ];
        if (isset($_POST['title'])) {
            $data['res'] = [
                'username' => $_SESSION['user']['username'],
                'writer' => $_POST['writer'],
                'title' => $_POST['title'],
                'news' => $_POST['news'],
            ];
            $response = $this->model('Blog_model')->addBlogData($data['res']);
            if ($response > 0) {
                exit(header('Location: ' . BASE_URL . '/blog'));
            } else {
                exit(header('Location: ' . BASE_URL . '/blog'));
            }
        } else {
            $this->view('templates/header', $data);
            $this->view('blog/add', $data);
            $this->view('templates/footer');
            exit;
        }
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Edit Blog',
            'res' => $this->model('Blog_model')->getBlogById($id)
        ];
        if (isset($_POST['id'])) {
            $data['res'] = [
                'id' => $_POST['id'],
                'username' => $_SESSION['user']['username'],
                'writer' => $_POST['writer'],
                'title' => $_POST['title'],
                'news' => $_POST['news'],
            ];
            $response = $this->model('Blog_model')->updateBlogData($data['res']);
            if ($response > 0) {
                exit(header('Location: ' . BASE_URL . '/blog'));
            } else {
                exit(header('Location: ' . BASE_URL . '/blog'));
            }
        } else {
            $this->view('templates/header', $data);
            $this->view('blog/edit', $data);
            $this->view('templates/footer');
            exit;
        }
    }

    public function delete($id)
    {
        $response = $this->model('Blog_model')->deleteBlogDataById($id);
        if ($response > 0) {
            echo 'ok';
            exit(header('Location: ' . BASE_URL . '/blog'));
        } else {
            echo 'failed';
            exit(header('Location: ' . BASE_URL . '/blog'));
        }
    }
}
