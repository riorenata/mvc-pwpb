<?php

class User extends Controller
{

  public function index()
  {
    if (!$this->model('User_model')->isLoggedIn()) exit(header('Location: ' . BASE_URL . '/user/signin'));
    $data = [
      'title' => 'User',
      'res' => $this->model('User_model')->getAllUsers(),
    ];
    $this->view('templates/header', $data);
    $this->view('user/index', $data);
    $this->view('templates/footer');
  }

  public function add()
  {
    $data = [
      'title' => 'Add User'
    ];
    if (isset($_POST['username'])) {
      $data['res'] = [
        'level' => 'user',
        'name' => $_POST['name'],
        'username' => $_POST['username'],
        'phone' => $_POST['phone'],
        'email' => $_POST['email'],
        'password' => $_POST['password'],
        // 'password2' => $_POST['password2'],
      ];
      $response = $this->model('User_model')->signup($data['res']);
      if ($response === true) {
        exit(header('Location: ' . BASE_URL . '/user'));
      } else {
        exit(header('Location: ' . BASE_URL . '/user'));
      }
    } else {
      $this->view('templates/header', $data);
      $this->view('user/add', $data);
      $this->view('templates/footer');
      exit;
    }
  }

  public function edit($id)
  {
    $data = [
      'title' => 'Edit User',
      'res' => $this->model('User_model')->getUserById($id)
    ];
    if (isset($_POST['id'])) {
      $data['res'] = [
        'id' => $_POST['id'],
        'level' => $_POST['level'],
        'name' => $_POST['name'],
        'username' => $_POST['username'],
        'phone' => $_POST['phone'],
        'email' => $_POST['email'],
        'password' => $_POST['password'],
      ];
      $response = $this->model('User_model')->updateUserData($data['res']);
      if ($response > 0) {
        exit(header('Location: ' . BASE_URL . '/user'));
      } else {
        exit(header('Location: ' . BASE_URL . '/user'));
      }
    } else {
      $this->view('templates/header', $data);
      $this->view('user/edit', $data);
      $this->view('templates/footer');
      exit;
    }
  }

  public function delete($id)
  {
    $response = $this->model('User_model')->deleteUserDataById($id);
    if ($response > 0) {
      echo 'ok';
      exit(header('Location: ' . BASE_URL . '/user'));
    } else {
      echo 'failed';
      exit(header('Location: ' . BASE_URL . '/user'));
    }
  }

  public function detail($id)
  {
    $data = [
      'title' => 'Profile',
      'res' => $this->model('User_model')->getUserById($id),
    ];
    $this->view('templates/header', $data);
    $this->view('user/detail', $data);
    $this->view('templates/footer');
  }

  public function signup()
  {
    $data = [
      'title' => 'Sign Up',
    ];
    if ($this->model('User_model')->isLoggedIn()) exit(header('Location: ' . BASE_URL . '/'));
    if (isset($_POST['username']) && isset($_POST['password'])) {
      if (strlen($_POST['username']) < 4 && strlen($_POST['password']) < 4) {
        exit(header('Location: ' . BASE_URL . '/user/signup'));
      }
      $args = [
        'name' => $_POST['name'],
        'username' => $_POST['username'],
        'phone' => $_POST['phone'],
        'email' => $_POST['email'],
        'password' => $_POST['password'],
        'password2' => $_POST['password2']
      ];
      $validate = $this->model('User_model')->signup($args);
      if ($validate === true) {
        exit(header('Location: ' . BASE_URL . '/'));
      } else {
        echo "<script>alert('Failed to signup!');</script>";
        $this->view('templates/header', $data);
        $this->view('user/signup', $data);
        $this->view('templates/footer');
        exit;
      }
    } else {
      $this->view('templates/header', $data);
      $this->view('user/signup', $data);
      $this->view('templates/footer');
      exit;
    }
  }

  public function signin()
  {
    $data = [
      'title' => 'Sign In'
    ];
    if ($this->model('User_model')->isLoggedIn()) exit(header('Location: ' . BASE_URL . '/'));
    if (isset($_POST['username']) && isset($_POST['password'])) {
      if (strlen($_POST['username']) < 4 && strlen($_POST['password']) < 4) {
        exit(header('Location: ' . BASE_URL . '/user/signin'));
      }
      $args = [
        'username' => $_POST['username'],
        'password' => $_POST['password']
      ];
      $validate = $this->model('User_model')->signin($args);
      if ($validate === true) {
        exit(header('Location: ' . BASE_URL . '/'));
      } else {
        echo "<script>alert('Incorrect username or password');</script>";
        $this->view('templates/header', $data);
        $this->view('user/signin', $data);
        $this->view('templates/footer');
        exit;
      }
    } else {
      $this->view('templates/header', $data);
      $this->view('user/signin', $data);
      $this->view('templates/footer');
      exit;
    }
  }

  public function signout()
  {
    if ($this->model('User_model')->isLoggedIn()) $this->model('User_model')->signout();
    exit(header('Location: ' . BASE_URL . '/user/signin'));
  }
}
