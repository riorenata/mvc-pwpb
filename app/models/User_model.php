<?php

class User_model
{
    private $db;
    private $table = 'users';

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getUserById($id)
    {
        $this->db->prepare("SELECT * FROM {$this->table} WHERE id=:id");
        $this->db->bindValue('id', $id);
        return $this->db->fetch();
    }

    public function getAllUsers()
    {
        $this->db->prepare("SELECT * FROM {$this->table}");
        return $this->db->fetchAll();
    }

    public function updateUserData($args)
    {
        $this->db->prepare("UPDATE {$this->table} SET
                            level=:level,
                            name=:name,
                            username=:username,
                            phone=:phone,
                            email=:email,
                            password=:password
                            WHERE id=:id");
        $this->db->bindValue('level', $args['level']);
        $this->db->bindValue('name', $args['name']);
        $this->db->bindValue('username', $args['username']);
        $this->db->bindValue('phone', $args['phone']);
        $this->db->bindValue('email', $args['email']);
        $this->db->bindValue('password', $args['password']);
        $this->db->bindValue('id', $args['id']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function deleteUserDataById($id)
    {
        $this->db->prepare("DELETE FROM {$this->table} WHERE id=:id");
        $this->db->bindValue('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function signup($data)
    {
        $md5P = md5($data['password'] . SALT);
        $this->db->prepare("INSERT INTO {$this->table}
                            VALUES
                            (null,'user',:name,:username,:phone,:email,:password)");
        $this->db->bindValue('name', $data['name']);
        $this->db->bindValue('username', $data['username']);
        $this->db->bindValue('phone', $data['phone']);
        $this->db->bindValue('email', $data['email']);
        $this->db->bindValue('password', $md5P);
        $this->db->execute();
        if ($this->db->rowCount() > 0) return true;
        else return false;
    }

    public function signin($data)
    {
        $this->db->prepare("SELECT * FROM {$this->table} WHERE username=:username");
        $this->db->bindValue('username', $data['username']);
        $res = $this->db->fetch();
        if ($this->db->rowCount() > 0) {
            $md5P = md5($data['password'] . SALT);
            if ($md5P == $res['password']) {
                $_SESSION = [
                    'user' => [
                        'id' => $res['id'],
                        'level' => $res['level'],
                        'name' => $res['name'],
                        'username' => $res['username']
                    ]
                ];
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function signout()
    {
        if (isset($_SESSION['user'])) session_destroy();
        unset($_SESSION['user']);
        return true;
    }

    public function isLoggedIn()
    {
        if (isset($_SESSION['user'])) return true;
    }
}
