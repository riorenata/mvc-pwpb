<?php

class Blog_model
{
    private $table = 'blog';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllBlog()
    {
        $this->db->prepare('SELECT * FROM blog');
        return $this->db->fetchAll();
    }

    public function getBlogById($id)
    {
        $this->db->prepare("SELECT * FROM {$this->table} WHERE id=:id");
        $this->db->bindValue('id', $id);
        return $this->db->fetch();
    }

    public function addBlogData($data)
    {
        $this->db->prepare("INSERT INTO {$this->table}
                            (id, username, writer, title, news)
                            VALUES
                            (null,:username,:writer,:title,:news)");
        $this->db->bindValue('username', $data['username']);
        $this->db->bindValue('writer', $data['writer']);
        $this->db->bindValue('title', $data['title']);
        $this->db->bindValue('news', $data['news']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function updateBlogData($data)
    {
        $this->db->prepare("UPDATE {$this->table} SET
                            username=:username,
                            writer=:writer,
                            title=:title,
                            news=:news
                            WHERE id=:id");
        $this->db->bindValue('username', $data['username']);
        $this->db->bindValue('writer', $data['writer']);
        $this->db->bindValue('title', $data['title']);
        $this->db->bindValue('news', $data['news']);
        $this->db->bindValue('id', $data['id']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function deleteBlogDataById($id)
    {
        $this->db->prepare("DELETE FROM {$this->table} WHERE id=:id");
        $this->db->bindValue('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }
}
