<?php

// host url
define('BASE_URL', 'http://localhost/mvc-pwpb');

// database credentials
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'pwpb');

// salt
define('SALT', 'o0a4fds!&$*^&@$=');
